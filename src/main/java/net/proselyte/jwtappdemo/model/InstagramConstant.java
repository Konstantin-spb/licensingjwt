package net.proselyte.jwtappdemo.model;

import javax.persistence.*;

@Entity
@Table(name = "instagram_constants")
public class InstagramConstant {
    /* Edited 10.05.2018
     *
     * Samsung Galaxy S7 Edge SM-G935F. Released: March 2016.
     * https://www.amazon.com/Samsung-SM-G935F-Factory-Unlocked-Smartphone/dp/B01C5OIINO
     * https://www.handsetdetection.com/properties/devices/Samsung/SM-G935F
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * API URL
     */
    @Column(name = "API_URL")
    private String apiUrl;

    /**
     * API Key (extracted from the apk)
     */
    @Column(name = "API_KEY")
    private String apiKey;

    /**
     * API Key Version
     */
    @Column(name = "API_KEY_VERSION")
    private String apiKeyVersion;

    /**
     * Device to mimic
     */
    @Column(name = "DEVICE_MANUFACTURER")
    private String deviceManufacturer;

    /**
     * Model to mimic
     */
    @Column(name = "DEVICE_MODEL")
    private String deviceModel;


    /**
     * Android version to mimic
     */
    @Column(name = "DEVICE_ANDROID_VERSION")
    private String deviceAndroidVersion;

    /**
     * Android Release
     */
    @Column(name = "DEVICE_ANDROID_RELEASE")
    private String deviceAndroidRelease;

    /**
     * Experiments Activated
     */
    @Column(name = "DEVICE_EXPERIMENTS", columnDefinition="TEXT")
    private String deviceExperiments;

    /**
     * Android Release
     */
    @Column(name = "USER_AGENT")
    private String userAgent;

    @Column(name = "simulator_version")
    private String simulatorVersion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKeyVersion() {
        return apiKeyVersion;
    }

    public void setApiKeyVersion(String apiKeyVersion) {
        this.apiKeyVersion = apiKeyVersion;
    }

    public String getDeviceManufacturer() {
        return deviceManufacturer;
    }

    public void setDeviceManufacturer(String deviceManufacturer) {
        this.deviceManufacturer = deviceManufacturer;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceAndroidVersion() {
        return deviceAndroidVersion;
    }

    public void setDeviceAndroidVersion(String deviceAndroidVersion) {
        this.deviceAndroidVersion = deviceAndroidVersion;
    }

    public String getDeviceAndroidRelease() {
        return deviceAndroidRelease;
    }

    public void setDeviceAndroidRelease(String deviceAndroidRelease) {
        this.deviceAndroidRelease = deviceAndroidRelease;
    }

    public String getDeviceExperiments() {
        return deviceExperiments;
    }

    public void setDeviceExperiments(String deviceExperiments) {
        this.deviceExperiments = deviceExperiments;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getSimulatorVersion() {
        return simulatorVersion;
    }

    public void setSimulatorVersion(String simulatorVersion) {
        this.simulatorVersion = simulatorVersion;
    }
}

