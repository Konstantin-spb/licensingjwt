package net.proselyte.jwtappdemo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.proselyte.jwtappdemo.model.InstagramConstant;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InstagramConstantDto {
    private String apiUrl;
    private String apiKey;
    private String apiKeyVersion;
    private String deviceManufacturer;
    private String deviceModel;
    private String deviceAndroidVersion;
    private String deviceAndroidRelease;
    private String deviceExperiments;
    private String userAgent;
    private String simulatorVersion;

    public InstagramConstantDto() {

    }

    public InstagramConstant toInstagramConstant() {
        InstagramConstant instagramConstant = new InstagramConstant();
        instagramConstant.setApiUrl(apiUrl);
        instagramConstant.setApiKey(apiKey);
        instagramConstant.setApiKeyVersion(apiKeyVersion);
        instagramConstant.setDeviceManufacturer(deviceManufacturer);
        instagramConstant.setDeviceModel(deviceModel);
        instagramConstant.setDeviceAndroidVersion(deviceAndroidVersion);
        instagramConstant.setDeviceAndroidRelease(deviceAndroidRelease);
        instagramConstant.setDeviceExperiments(deviceExperiments);
        instagramConstant.setUserAgent(userAgent);
        instagramConstant.setSimulatorVersion(simulatorVersion);
        return instagramConstant;
    }

    public static InstagramConstantDto fromInstagramConstantDto(InstagramConstant instagramConstant) {
        InstagramConstantDto instagramConstantDto = new InstagramConstantDto();
        instagramConstantDto.setApiUrl(instagramConstant.getApiUrl());
        instagramConstantDto.setApiKey(instagramConstant.getApiKey());
        instagramConstantDto.setApiKeyVersion(instagramConstant.getApiKeyVersion());
        instagramConstantDto.setDeviceManufacturer(instagramConstant.getDeviceManufacturer());
        instagramConstantDto.setDeviceModel(instagramConstant.getDeviceModel());
        instagramConstantDto.setDeviceAndroidVersion(instagramConstant.getDeviceAndroidVersion());
        instagramConstantDto.setDeviceAndroidRelease(instagramConstant.getDeviceAndroidRelease());
        instagramConstantDto.setDeviceExperiments(instagramConstant.getDeviceExperiments());
        instagramConstantDto.setUserAgent(instagramConstant.getUserAgent());
        instagramConstantDto.setSimulatorVersion(instagramConstant.getSimulatorVersion());
        return instagramConstantDto;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKeyVersion() {
        return apiKeyVersion;
    }

    public void setApiKeyVersion(String apiKeyVersion) {
        this.apiKeyVersion = apiKeyVersion;
    }

    public String getDeviceManufacturer() {
        return deviceManufacturer;
    }

    public void setDeviceManufacturer(String deviceManufacturer) {
        this.deviceManufacturer = deviceManufacturer;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceAndroidVersion() {
        return deviceAndroidVersion;
    }

    public void setDeviceAndroidVersion(String deviceAndroidVersion) {
        this.deviceAndroidVersion = deviceAndroidVersion;
    }

    public String getDeviceAndroidRelease() {
        return deviceAndroidRelease;
    }

    public void setDeviceAndroidRelease(String deviceAndroidRelease) {
        this.deviceAndroidRelease = deviceAndroidRelease;
    }

    public String getDeviceExperiments() {
        return deviceExperiments;
    }

    public void setDeviceExperiments(String deviceExperiments) {
        this.deviceExperiments = deviceExperiments;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getSimulatorVersion() {
        return simulatorVersion;
    }

    public void setSimulatorVersion(String simulatorVersion) {
        this.simulatorVersion = simulatorVersion;
    }
}
