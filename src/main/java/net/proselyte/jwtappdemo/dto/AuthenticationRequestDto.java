package net.proselyte.jwtappdemo.dto;

import java.util.Objects;

/**
 * DTO class for authentication (login) request.
 *
 * @author Eugene Suleimanov
 * @version 1.0
 */

public class AuthenticationRequestDto {
    private String username;
    private String password;
    private String email;

    public AuthenticationRequestDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    protected boolean canEqual(final Object other) {
        return other instanceof AuthenticationRequestDto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthenticationRequestDto that = (AuthenticationRequestDto) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(password, that.password) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, email);
    }

    public String toString() {
        return "AuthenticationRequestDto(username=" + this.getUsername() + ", password=" + this.getPassword() + ")";
    }
}
