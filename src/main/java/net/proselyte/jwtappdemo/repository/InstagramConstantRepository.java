package net.proselyte.jwtappdemo.repository;

import net.proselyte.jwtappdemo.model.InstagramConstant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstagramConstantRepository extends JpaRepository<InstagramConstant, Long> {

    InstagramConstant findBySimulatorVersion(String simulatorVersion);
}
