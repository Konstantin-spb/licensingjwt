package net.proselyte.jwtappdemo.repository;

import net.proselyte.jwtappdemo.model.History;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoryRepository extends JpaRepository<History, Long> {
}
