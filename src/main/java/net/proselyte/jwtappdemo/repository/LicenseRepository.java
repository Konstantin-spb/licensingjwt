package net.proselyte.jwtappdemo.repository;

import net.proselyte.jwtappdemo.model.License;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LicenseRepository extends JpaRepository<License, Long> {
}
