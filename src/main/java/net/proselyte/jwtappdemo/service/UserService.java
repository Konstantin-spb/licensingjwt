package net.proselyte.jwtappdemo.service;

import net.proselyte.jwtappdemo.model.User;

import java.util.List;

/**
 * Service interface for class {@link User}.
 *
 * @author Eugene Suleimanov
 * @version 1.0
 */

public interface UserService {

    User register(User user);

    List<User> getAll();

    User findByUsername(String username);

    User findByUsernameAndEmail(String username, String email);

    User findById(Long id);

    void delete(Long id);
}
