package net.proselyte.jwtappdemo.service;

import net.proselyte.jwtappdemo.model.History;
import net.proselyte.jwtappdemo.model.User;

public interface HistoryService {
    History saveActionHistory(User user, String action, String ipAddress);
}
