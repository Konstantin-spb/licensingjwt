package net.proselyte.jwtappdemo.service.impl;

import net.proselyte.jwtappdemo.model.License;
import net.proselyte.jwtappdemo.model.User;
import net.proselyte.jwtappdemo.repository.LicenseRepository;
import net.proselyte.jwtappdemo.service.LicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

@Service
public class LicenseServiceImpl implements LicenseService {
@Autowired
private LicenseRepository licenseRepository;

    @Override
    public License createNewLicense(User user) {
        License license = new License();
        license.setUser(user);
        license.setCpuId(user.getUsername());
        license.setStartDate(new Timestamp(System.currentTimeMillis()));
        license.setEndDate(new Timestamp(System.currentTimeMillis() + 604800000));
        license.setLicenseStatus("DEMO");
        licenseRepository.save(license);
        return license;
    }

    @Override
    public License checkLicense() {
        return null;
    }
}
