package net.proselyte.jwtappdemo.service.impl;

import net.proselyte.jwtappdemo.model.History;
import net.proselyte.jwtappdemo.model.User;
import net.proselyte.jwtappdemo.repository.HistoryRepository;
import net.proselyte.jwtappdemo.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    HistoryRepository historyRepository;

    @Override
    @Transactional
    public History saveActionHistory(User user, String action, String ipAddress) {
        History history = new History();
        history.setAction(action);
        history.setDateAction(new Date());
        history.setIpAddress(ipAddress);
        history.setUser(user);
        return historyRepository.save(history);
    }
}
