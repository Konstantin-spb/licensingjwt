package net.proselyte.jwtappdemo.service;

import net.proselyte.jwtappdemo.model.License;
import net.proselyte.jwtappdemo.model.User;

public interface LicenseService {

    License createNewLicense(User user);

    License checkLicense();
}
