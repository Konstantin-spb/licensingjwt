package net.proselyte.jwtappdemo.rest;

import net.proselyte.jwtappdemo.dto.AuthenticationRequestDto;
import net.proselyte.jwtappdemo.dto.InstagramConstantDto;
import net.proselyte.jwtappdemo.model.InstagramConstant;
import net.proselyte.jwtappdemo.model.User;
import net.proselyte.jwtappdemo.repository.InstagramConstantRepository;
import net.proselyte.jwtappdemo.security.jwt.JwtTokenProvider;
import net.proselyte.jwtappdemo.service.HistoryService;
import net.proselyte.jwtappdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * REST controller for authentication requests (login, logout, register, etc.)
 *
 * @author Eugene Suleimanov
 * @version 1.0
 */

@RestController
@RequestMapping(value = "/api/v1/auth/")
public class AuthenticationRestControllerV1 {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;
    private final HistoryService historyService;
    private final InstagramConstantRepository instagramConstantRepository;

    @Autowired
    public AuthenticationRestControllerV1(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserService userService, HistoryService historyService, InstagramConstantRepository instagramConstantRepository) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
        this.historyService = historyService;
        this.instagramConstantRepository = instagramConstantRepository;
    }

    @GetMapping("login")
    public String login() {
        return "( .)(. )";
    }

    @PostMapping("login")
    public ResponseEntity login(@RequestBody AuthenticationRequestDto requestDto, HttpServletRequest request) {
        System.out.println();
            if(requestDto == null || requestDto.getUsername() == null || requestDto.getUsername().isEmpty() || requestDto.getPassword() == null || requestDto.getPassword().isEmpty()
                    || requestDto.getEmail() == null || requestDto.getEmail().isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

        try {
            String username = requestDto.getUsername();
            String email = requestDto.getEmail();
            User user = userService.findByUsername(username);
//            User user = userService.findByUsernameAndEmail(username, email);
            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + username + " not found");
            }

            historyService.saveActionHistory(user, "login", request.getRemoteAddr());
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, requestDto.getPassword()));
            String token = jwtTokenProvider.createToken(username, user.getRoles());

            Map<Object, Object> response = new HashMap<>();
            response.put("username", username);
            response.put("token", token);
            response.put("timestamp", user.getLicense().getEndDate());

            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
//            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("constants/{id}")
    public ResponseEntity constants(@PathVariable(name = "id") String id, HttpServletRequest request) {
        InstagramConstant instagramConstant = instagramConstantRepository.findBySimulatorVersion(id);
        if(instagramConstant == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        InstagramConstantDto result = InstagramConstantDto.fromInstagramConstantDto(instagramConstant);
        result.setApiKey("2" + result.getApiKey() + "a");
//        result.setApiKeyVersion(result.getApiKeyVersion() + "a");
//        result.setApiUrl(result.getApiUrl() + "a");
//        result.setDeviceAndroidRelease(result.getDeviceAndroidRelease() + "a");
//        result.setDeviceAndroidVersion(result.getDeviceAndroidVersion() + "a");
//        result.setDeviceExperiments(result.getDeviceExperiments() + "a");
//        result.setDeviceManufacturer(result.getDeviceManufacturer() + "a");
//        result.setDeviceModel(result.getDeviceModel() + "a");
//        result.setSimulatorVersion(result.getSimulatorVersion() + "a");
//        result.setUserAgent(result.getUserAgent() + "a");

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
