package net.proselyte.jwtappdemo.rest;

import net.proselyte.jwtappdemo.dto.AuthenticationRequestDto;
import net.proselyte.jwtappdemo.model.License;
import net.proselyte.jwtappdemo.model.User;
import net.proselyte.jwtappdemo.repository.UserRepository;
import net.proselyte.jwtappdemo.service.HistoryService;
import net.proselyte.jwtappdemo.service.LicenseService;
import net.proselyte.jwtappdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;

/**
 * REST controller user connected requestst.
 *
 * @author Eugene Suleimanov
 * @version 1.0
 */

@RestController
@RequestMapping(value = "/api/v1/users/")
public class UserRestControllerV1 {
    private final UserService userService;
    private final UserRepository userRepository;
    private final HistoryService historyService;
    private final LicenseService licenseService;


    @Autowired
    public UserRestControllerV1(UserService userService, UserRepository userRepository, HistoryService historyService, LicenseService licenseService) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.historyService = historyService;
        this.licenseService = licenseService;
    }

//    @GetMapping(value = "{id}")
//    public ResponseEntity<UserDto> getUserById(@PathVariable(name = "id") Long id){
//        User user = userService.findById(id);
//
//        if(user == null){
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//
//        UserDto result = UserDto.fromUser(user);
//
//        return new ResponseEntity<>(result, HttpStatus.OK);
//    }

    @PostMapping("/registration")
    public ResponseEntity registration(@RequestBody AuthenticationRequestDto userForm, HttpServletRequest request) {
        ArrayList<String> registrationStatus = new ArrayList<>();
        registrationStatus.add("user was created");
        registrationStatus.add("such user is already in the database");

        String answer = registrationStatus.get(1);

        if(userForm == null || userForm.getUsername() == null || userForm.getUsername().isEmpty() || userForm.getPassword() == null || userForm.getPassword().isEmpty()
        || userForm.getEmail() == null || userForm.getEmail().isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        User user = userRepository.findByUsername(userForm.getUsername());
        if (user == null) {
            user = new User();
//            userForm.setUsername(userForm.getUsername());
//            userForm.setPassword(userForm.getPassword());
            user.setUsername(userForm.getUsername());
            user.setPassword(userForm.getPassword());
            user.setEmail(userForm.getEmail());
            user.setCreated(new Date());

//            license.setCpuId(user.getUsername());
//            license.

            try {
                user = userService.register(user);
                License license = licenseService.createNewLicense(user);

            } catch (Exception e) {
                e.printStackTrace();
            }
            answer = registrationStatus.get(0);
        }
//        UserDto result = UserDto.fromUser(user);

        historyService.saveActionHistory(user, "registration", request.getRemoteAddr());
        return new ResponseEntity(answer, HttpStatus.OK);
    }
}
