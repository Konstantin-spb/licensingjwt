//package net.proselyte.jwtappdemo.rest;
//
//import net.proselyte.jwtappdemo.dto.InstagramConstantDto;
//import net.proselyte.jwtappdemo.model.InstagramConstant;
//import net.proselyte.jwtappdemo.repository.InstagramConstantRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//
//@RestController
//@RequestMapping(value = "/api/v1/inn/")
//public class InnController {
//    @Autowired
//    private InstagramConstantRepository instagramConstantRepository;
//
//    @PostMapping("constants/{id}")
//    public ResponseEntity constants(@PathVariable(name = "id") String id, HttpServletRequest request) {
//        InstagramConstant instagramConstant = instagramConstantRepository.findBySimulatorVersion(id);
//        if(instagramConstant == null){
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//        InstagramConstantDto result = InstagramConstantDto.fromInstagramConstantDto(instagramConstant);
//        return new ResponseEntity<>(result, HttpStatus.OK);
//    }
//}
