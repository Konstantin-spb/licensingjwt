package net.proselyte.jwtappdemo.rest;

import net.proselyte.jwtappdemo.dto.AdminUserDto;
import net.proselyte.jwtappdemo.model.InstagramConstant;
import net.proselyte.jwtappdemo.model.User;
import net.proselyte.jwtappdemo.repository.InstagramConstantRepository;
import net.proselyte.jwtappdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * REST controller for ROLE_ADMIN requests.
 *
 * @author Eugene Suleimanov
 * @version 1.0
 */

@RestController
@RequestMapping(value = "/api/v1/admin/")
public class AdminRestControllerV1 {
    @Autowired

        private final UserService userService;
    @Autowired
    public AdminRestControllerV1(UserService userService) {
        this.userService = userService;
    }
//
    @GetMapping(value = "users/{id}")
    public ResponseEntity<AdminUserDto> getUserById(@PathVariable(name = "id") Long id) {
        User user = userService.findById(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        AdminUserDto result = AdminUserDto.fromUser(user);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
